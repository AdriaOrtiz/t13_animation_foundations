﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller180Alfa : MonoBehaviour
{

    public Animator anim;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("b"))
        {
            anim.Play("Take_HandShacke");
        }
    }
}
