﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitySword : MonoBehaviour {

    public Rigidbody rigidbody;

    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("c"))
        {
            rigidbody.useGravity = true;
        }
	}
}
