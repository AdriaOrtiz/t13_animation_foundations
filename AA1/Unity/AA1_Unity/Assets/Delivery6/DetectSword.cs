﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectSword : MonoBehaviour {

    public GameObject Hand;
    public GameObject _sword;

    public Vector3 PickPosition;
    public Vector3 PickRotation;

    private bool isColliding = false;

		void OnCollisionEnter(Collision col)
        {
        if (col.gameObject.name == "Sword")
        {
            Debug.Log("Collision with + " + col.gameObject.name);

            _sword = col.gameObject;

            //col.gameObject.transform.parent = Hand.transform;
           // col.gameObject.transform.localPosition = PickPosition;
           // col.gameObject.transform.localEulerAngles = PickRotation;
            //Debug.Log("Position = " + col.gameObject.transform.localPosition);

            isColliding = true;
        }
            
        }

    void Update()
    {
        if (isColliding)
        {
            _sword.gameObject.transform.parent = Hand.transform;
            _sword.gameObject.transform.localPosition = PickPosition;
            _sword.gameObject.transform.localEulerAngles = PickRotation;
            Debug.Log("Position = " + _sword.gameObject.transform.localPosition);
        }
    }
}
