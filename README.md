# Team description 

#### Name of the team:  t13_animation_foundations

#### Repository Addres: https://gitlab.com/AdriaOrtiz/t13_animation_foundations

#### Couple number: 13

#### Number of students in the team: 1

---------------------
#### Components Name: Adrià Ortiz Navarro
#### Email: adriaortiznavarro@enti.cat
#### Pic: ![alt text](https://gitlab.com/AdriaOrtiz/t13_animation_foundations/uploads/27fd6ea6d986e0f4bc8519b3b446367f/foto.jpg) 
#### Gitlab account: AdriaOrtiz


# Delivery 1:

### Exercicse 1: 

![alt text](https://gitlab.com/AdriaOrtiz/t13_animation_foundations/uploads/63b7203c18d39a3ba62bbc67d74bcfa6/Delivery_1.png)

### Exercicse 2: 
Steps:
1. Make merge of the animations
2. Charaterize with the correct bones
3. Make a layer to correct the feet, which were too turned
4. Make an additional layer to correct the movement of the character's hand without a suit. It was the hardest part.

![alt text](https://gitlab.com/AdriaOrtiz/t13_animation_foundations/uploads/192ea40ed4fa93f62f6507c251cd788f/Delivery2.gif)

Reverse view:

![alt text](https://gitlab.com/AdriaOrtiz/t13_animation_foundations/uploads/0e94fb01d57bc3a5dfeafe977f3517af/Delivery2_Reverse.gif)

### Exercicse 3: 

Steps:
1. Import the animations to Unity (The hardest part, because i lost a lot of time exporting bad the animations and losing them in Unity)
2. Create the animations controllers
3. Create the inuput script in Unity 
4. Record the result 

![alt text](https://gitlab.com/AdriaOrtiz/t13_animation_foundations/uploads/1a7f8eb26bdeb329c70e3bf7d75993d6/Delivery3.gif)

### Exercicse 4: 

Steps: 
1. Import the 150cm character and the TeddyBear
2. Characterize both characters
3. Correct with keys the right arm of both characters, so that it does not cross their chest

TeddyBear+180cm:

![alt text](https://gitlab.com/AdriaOrtiz/t13_animation_foundations/uploads/4afc34f32b5246a1519f4dbeaa305ae9/Delivery4_Teddy_180.gif)

150cm+180cm:

![alt text](https://gitlab.com/AdriaOrtiz/t13_animation_foundations/uploads/c109b923d57c406e47e8bf75f3fc3714/Delivery4_150_180.gif)

### Exercicse 5:

Steps: 
1. Import the animations to Unity
2. Create the animations controllers
3. Create the inuput script in Unity 
4. Record the results 

The steps were very similar to Exercicse 3 and there were no difficulties

TeddyBear+180cm:
![alt text](https://gitlab.com/AdriaOrtiz/t13_animation_foundations/uploads/086c7f8c174f72b8ac0951c865b1bd79/Delivery5_Teddy_180.gif)

150+180:
![alt text](https://gitlab.com/AdriaOrtiz/t13_animation_foundations/uploads/30f7ca3c41b6e0c0c380e235c599f5ad/Delivery5_150_180.gif)



### Exercicse 6:

Steps:
1. Edit the animation in MotionBuilder
2. Export the animation to Unity
3. Import a sword model to Unity
4. Make the sword fall
5. Create a collision system with the Character and the Sword (The hardest part by far)
6. Attach the sword into the hand of the character (ponderous)
7. Make the input activate all

ZoomOff:
![alt text](https://gitlab.com/AdriaOrtiz/t13_animation_foundations/uploads/df52990fbbdb610f21a88a7c8ad486ca/Delivery6_NoZoom.gif)

ZoomOn:
![alt text](https://gitlab.com/AdriaOrtiz/t13_animation_foundations/uploads/265fc7b326a07d8eac3f189e90a04798/Delivery6_Zoom.gif))


# Delivery2: 

### Exercicse 1:

   1. MyVector only uses AxisAngleToEuler, a method that transforms the axisAngle normalized and the angle to a EulerAngle

   2. Capture: ![alt text](https://gitlab.com/AdriaOrtiz/t13_animation_foundations/uploads/f034597aaca3d092372f54d41a061fc8/Exercice1.png)

   3. 	LocalRotation: control1-turnme! (0.0, 0.6, 0.7, 0.4)
	LocalRotation: control2-turnme! (0.3, 0.3, 0.7, 0.7)
	LocalRotation: control3-turnme! (0.8, -0.3, 0.4, 0.4)

### Exercice 2:

   1. 	I make that JOINT1.localEulerAngles = controller1.localEulerAngles, so the controller rotations affects to Joint rotations
	With the function LocalEulerWithConstrains, i convert a localEulerAngle into a AxisAngle, then 
	get the angle, and convert the Quaternion into a localEulerAngle  (constraining the all the 3 axis)

   2. 	Capture (Constrain of MinAngle = 10 and MaxAngle = 45): ![alt text](https://gitlab.com/AdriaOrtiz/t13_animation_foundations/uploads/86ca4231392bcdc7330545272801c2a2/Exercice2.gif)

   3.   LocalRotation: JOINT1 (-0.1, 0.1, 0.1, 1.0)
        LocalRotation: JOINT2 (0.5, 0.0, 0.2, 0.8)
	    LocalRotation: JOINT3 (0.2, 0.2, 0.3, 0.9)

### Exercice 3:

First, with this exercice, there are 2 public bools, that can turn on or off both of them.

   1. 	First i get the localEulerAngles from the controlTurnme!
        Then, in myQuaternion class with the localEulerAngles and the axis (a vector3D) that matter (depends to the joint, but is for not rotating in directions that are 
        imposible in real life), i transform the EulerAngles into a AxisAngle, and then, multiply the components for the axis (because there's only 1 axis that matters).
        With this i get the twist. The twist is a quaternion, so i need to convert it back into a EulerAngle with the function TwistQuaternionToEuler. 
        Finally, with the result of this function, i set the localEulerAngles. 
        
        Next, is the switch. First, i do get the twist, like i explained before. Next, i get the localEulerAngles of the JointX, and transform it into a Quaternion.
        With the twist Quaternion, i conjugate it with the function MyConjugateQuaternion. 
        Next, i multiply the the result of MyConjugateQuaternion with the localEulerAngles in Quanternion form. The result is the swing. 

   2. 	The only diference that i can see, is that in the twist, the Joints only rotates in 1 axis. 


# Delivery3: 



   